import UIKit
import MapKit

class ViewController: UIViewController,CLLocationManagerDelegate, UITextFieldDelegate {

    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var createAccount: UIButton!
    @IBOutlet weak var forgotPassword: UIButton!
    

    @IBOutlet weak var wifi: UIImageView!
    
    
    var locationManager = CLLocationManager()
    
    //Called when main view is loaded
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //Ask the user to allow this app to access on his location
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        DataBaseController.initializeDataBase()
        
        //To make the connect button rounded
        connectButton.layer.cornerRadius = 10
        connectButton.clipsToBounds = true
        
        //Hide before internet avalability
        connectButton.isHidden = true
        userNameField.isHidden = true
        passwordField.isHidden = true
        
        //Check is device is online or server are too
        checkInternet()

    }
    
    func checkInternet()
    {
 
        NetworkController.refreshOnlineStatut {
            
            //If network is online
            if(NetworkController.online)
            {
                if(DataBaseController.databaseHasData())
                {
                    self.tryToLogWithLocalData()
                }
                else
                {
                    DispatchQueue.main.async(execute: {self.registerButtonAndFieldAction()})
                }
                
            }
            //if user is offline but have some data
            else if(DataBaseController.databaseHasData())
            {
                let alert = UIAlertController(title: "Network error", message: "Can't reach online server, but you have local data.\nDo you want to use them or retry to connect ?", preferredStyle: UIAlertControllerStyle.alert)
                //Do nothing
                alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (action: UIAlertAction!) in
                   self.checkInternet()
                }))
                
                alert.addAction(UIAlertAction(title: "Use local data", style: .default, handler: { (action: UIAlertAction!) in
                    
                    FamilyController.reloadFamilyFromLocal
                    {
                            self.goToMainScreen()
                    }
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
            //we can't do anything cause we havent local data and no network
            else
            {
                let alert = UIAlertController(title: "Network error", message: "Please login with internet connexion at the first time.\nMaybe MyFamily server are offline or you haven't internet", preferredStyle: UIAlertControllerStyle.alert)
                //Do nothing
                alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (action: UIAlertAction!) in
                    self.checkInternet()
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }

    
    func registerButtonAndFieldAction()
    {
        wifi.isHidden = true
        connectButton.isHidden = false
        userNameField.isHidden = false
        passwordField.isHidden = false
        
        //Add a listener on the button to catch click action
        connectButton.addTarget(self, action: #selector(didButtonClick), for: .touchUpInside)
        createAccount.addTarget(self, action: #selector(didButtonClick), for: .touchUpInside)
        forgotPassword.addTarget(self, action: #selector(didButtonClick), for: .touchUpInside)
        
        self.userNameField.delegate = self
        self.passwordField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //Hide keybord when done is used
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField==userNameField)
        {
            passwordField.becomeFirstResponder()  
        }
        
        if(textField==passwordField)
        {
            self.view.endEditing(true)
        }
        
        return false
    }
    
    
    //on button connect click function
    @objc func didButtonClick(_ sender: UIButton) {
        
        switch sender {
        case connectButton:
            tryToLog()
        case createAccount:
            guard let url = URL(string: "http://supinfo.steve-colinet.fr/supfamily/") else { return }
            UIApplication.shared.open(url)
        case forgotPassword:
            guard let url = URL(string: "http://supinfo.steve-colinet.fr/supfamily/") else { return }
            UIApplication.shared.open(url)
            
        default:
            print("unknow button")
        }
 
    }
    
    //Try to log the user
    func tryToLog()
    {
        let username =  userNameField.text!
        let password = passwordField.text!
        
        //Avoid app crash when call rest api
        if username.contains(" ") || password.contains(" ")
        {
            self.checkInternet()
            if(NetworkController.online)
            {
                self.displayLoginError()
            }
            else
            {
                self.displayhttpError()
            }
            return
        }
        

        //Try to log with credidentials
        FamilyController.tryToLog(user: username ,password: password, callback: { (loginSuccess : Bool) in
   
            if(loginSuccess)
            {
                //Success login
                FamilyController.reloadFamilyMembers
                {
                        self.goToMainScreen()
                }
            }
            else
            {
                self.checkInternet()
                if(NetworkController.online)
                {
                    self.displayLoginError()
                }
                else
                {
                    self.displayhttpError()
                }
                
            }
        })

    }
    
    
    //Try to log the user from the local db data
    func tryToLogWithLocalData()
    {
        print("Try to log from local data")
        let credidentials = DataBaseController.getAuthenticate()
        CredidentialController.user=credidentials[0]
        CredidentialController.password=credidentials[1]
        
        let username = CredidentialController.user
        let password = CredidentialController.password
        
        
        //Try to log with credidentials
        FamilyController.tryToLog(user: username ,password: password, callback: { (loginSuccess : Bool) in
            
            if(loginSuccess)
            {
                //Success login
                FamilyController.reloadFamilyMembers
                {
                        self.goToMainScreen()
                }
            }
            else
            {
              
                DispatchQueue.main.async(execute: {
                    //Create pop up to remove member
                    let alert = UIAlertController(title: "Loggin error", message: "Your local credidentials seems to be outdated, they will be erased and the app will restart.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    //Clear local data
                    alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { (action: UIAlertAction!) in
                        DataBaseController.deleteAllRows(clearAuth: true)
                        exit(0)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                })
                
            }
        })
        
    }
    
    
    
 
    
    
    
    
    //go to main screen with internetdata or local
    func goToMainScreen()
    {
        DispatchQueue.main.async(execute:
        {
                self.performSegue(withIdentifier: "ConnectSegue", sender: self)
        })
    }
    
    
    //Prompt error when login
    func displayLoginError()
    {
        //To execute ui popup on main thread
        DispatchQueue.main.async(execute: {
            //Create pop up to remove member
            let alert = UIAlertController(title: "Loggin error", message: "Please verify you credentials", preferredStyle: UIAlertControllerStyle.alert)
            //Do nothing
            alert.addAction(UIAlertAction(title: "ok", style: .cancel))
            
            self.present(alert, animated: true, completion: nil)
        })
        

    }
    
    //Prompt error when login and http is down
    func displayhttpError()
    {
        if DataBaseController.databaseHasData()
        {
            DispatchQueue.main.async(execute: {
                //Create pop up to remove member
                let alert = UIAlertController(title: "Network error", message: "You lost your internet connectivity but you have local data. Restart app to use them", preferredStyle: UIAlertControllerStyle.alert)
                //Do nothing
                alert.addAction(UIAlertAction(title: "ok", style: .cancel))
                
                self.present(alert, animated: true, completion: nil)
            })
        }
        else
        {
            //To execute ui popup on main thread
            DispatchQueue.main.async(execute: {
                //Create pop up to remove member
                let alert = UIAlertController(title: "Network error", message: "You lost your internet connectivity and you have no local data. Please retry later", preferredStyle: UIAlertControllerStyle.alert)
                //Do nothing
                alert.addAction(UIAlertAction(title: "ok", style: .cancel))
                
                self.present(alert, animated: true, completion: nil)
            })
            
        }

        
    }

}

