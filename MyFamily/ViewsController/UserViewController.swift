//
//  UserViewController.swift
//  MyFamily
//
//  Created by Supinfo on 01/12/2018.
//  Copyright © 2018 Supinfo. All rights reserved.
//

import UIKit

class UserViewController : UIViewController
{
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var familyLabel: UILabel!
    @IBOutlet weak var onlineLabel: UILabel!
    
    @IBOutlet weak var logoutButton: UIButton!
    
    static var instance : UserViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserViewController.instance = self
        drawScreenWithData()
    }
    @IBAction func onButtonClick(_ sender: Any) {
      CredidentialController.logOut()
    }
    


    
    //draw the screen with data loaded
    public func drawScreenWithData()
    {
        welcomeLabel.text="Welcome back \(FamilyController.getUser().firstName ?? "notLoaded")"
        
        let number = String(FamilyController.getFamily().members!.count)
        
        familyLabel.text="Your family has \(number) members"
        
        onlineLabel.numberOfLines=0
        if(NetworkController.online)
        {
            onlineLabel.text="You are online, \nyour family was updated from web !"
        }
        else
        {
            onlineLabel.text="You are offline, location for your \nfamily's members will not be updated !"
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
