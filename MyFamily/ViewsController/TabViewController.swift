import UIKit


class TabViewController: UITabBarController {

    
    //Called when tab view is loaded after success login
    //This class is called for all the tabs
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
