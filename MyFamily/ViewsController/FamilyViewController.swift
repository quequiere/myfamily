import UIKit

class FamilyViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate
{

    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var plusButton: UIButton!
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    
    
    //define a cell
    let cellReuseIdentifier = "cell"
    
    //Define a table view
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //register button event
        plusButton.addTarget(self, action: #selector(didButtonClick), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(didButtonClick), for: .touchUpInside)
        addButton.addTarget(self, action: #selector(didButtonClick), for: .touchUpInside)
        
        //round and hide popup view
        popupView.isHidden=true
        popupView.layer.cornerRadius = 10

        //Register the tab view
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        
        self.firstNameField.delegate = self
        self.lastNameField.delegate = self
   

    }
    
    //Get the number of row
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FamilyController.getFamily().getMembers().count
    }
    
    //Create cell for each row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //create a cell or use an old cell
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
        
        //get the member
        let member = FamilyController.getFamily().getMembers()[indexPath.row]
        
        //Build the string to display
        let toDisplay = member.getName()
        
        //Set the cell text
        cell.textLabel?.text = toDisplay
        
        return cell
    }
    
    //Run on row click
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Cancel click when popup is prompt
        if !popupView.isHidden{
            return
        }
        
        
        //Create pop up to remove member
        let alert = UIAlertController(title: "Remove member", message: "Do you want to remove this member ?", preferredStyle: UIAlertControllerStyle.alert)
        
        //Remove the member
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.viewDidLoad()
     
            //Check if user want to remove himself from memberList
            if(FamilyController.getFamily().getMember(pos: indexPath.row).userId == FamilyController.getUser().userId)
            {
                 let stopAlert = UIAlertController(title: "Can't do that", message: "You can't remove yourself from your own family !", preferredStyle: UIAlertControllerStyle.alert)
                stopAlert.addAction(UIAlertAction(title: "Oops, nice try ^^", style: .cancel))
                    self.present(stopAlert, animated: true, completion: nil)
            }
            else
            {
                //Remove a member of the family
                FamilyController.getFamily().removeMember(pos: indexPath.row)
                self.reloadDataAfterChange()
            }

            
            
        }))
        
        //Do nothing
        alert.addAction(UIAlertAction(title: "No", style: .cancel))
        
        present(alert, animated: true, completion: nil)
    }
    
    
    //on button connect click function
    @objc func didButtonClick(_ sender: UIButton) {
        switch sender {
        case plusButton:
            popupView.isHidden = !popupView.isHidden
        case cancelButton:
            firstNameField.text=""
            lastNameField.text=""
            popupView.isHidden = true
        case addButton:
            tryToAddNewMember()
        default:
            print("unknow button")
        }
        
    }
    
    //To add member from popup menu
    func tryToAddNewMember()
    {
        if let fname = firstNameField.text, let lname = lastNameField.text
        {
            if(!fname.isEmpty && !lname.isEmpty)
            {
                let member : Member = Member(firstName: fname, lastName: lname)
                FamilyController.getFamily().addMember(member: member)
                popupView.isHidden = true
                firstNameField.text=""
                lastNameField.text=""
                self.view.endEditing(true)
                reloadDataAfterChange()
                return
            }
        }
        
        let alert = UIAlertController(title: "Input error", message: "Please provide firstname and lastname", preferredStyle: UIAlertControllerStyle.alert)
        //Do nothing
        alert.addAction(UIAlertAction(title: "close", style: .cancel, handler: { (action: UIAlertAction!) in }))
    }
    
    //Reloaded needed screen
    func reloadDataAfterChange()
    {
        tableView.reloadData()
        
        if let mapView = MapViewController.instance{
            mapView.refreshAllAnotation()
        }
        
        if let userView = UserViewController.instance{
            userView.drawScreenWithData()
        }
    }
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
