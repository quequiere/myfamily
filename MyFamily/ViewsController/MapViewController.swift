import UIKit
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    public static var instance : MapViewController?

    //Called when map view is loaded
    override func viewDidLoad() {

        super.viewDidLoad()
        MapViewController.instance=self
        
        self.refreshAllAnotation()

        //To display compas
        mapView.showsCompass=true
        
        

    }
    
    //Remove all pins to refresh them
    func refreshAllAnotation()
    {
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        
        
        //For all member
        for member in FamilyController.getFamily().getMembers()
        {
            if member.latitude==nil || member.longitude==nil
            {
                continue
            }
            
            //Create pin
            let pin = MKPointAnnotation()
            
            //Add his location
            pin.coordinate = CLLocationCoordinate2D(latitude: member.latitude!, longitude: member.longitude!)
            
            //Set to the member name
            pin.title = member.getName()
            
            //Added pin to map
            mapView.addAnnotation(pin)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


