class DataBaseQuery
{
    
    // ------- Create
    
    static let memberTableCreate :String =
    "CREATE TABLE IF NOT EXISTS member (id INTEGER PRIMARY KEY AUTOINCREMENT, firstName VARCHAR(255), lasname VARCHAR(255), userId INTEGER, latitude DOUBLE, longitude DOUBLE)"
    
    static let userTableCreate :String =
    "CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, firstName VARCHAR(255), lasname VARCHAR(255), userId INTEGER)"
    
    static let familyTableCreate :String =
    "CREATE TABLE IF NOT EXISTS family (id INTEGER PRIMARY KEY AUTOINCREMENT, familyId INTEGER, name VARCHAR(255))"
    
    static let authenticateTableCreate :String =
    "CREATE TABLE IF NOT EXISTS authenticate (id INTEGER PRIMARY KEY AUTOINCREMENT, user VARCHAR(255), password VARCHAR(255))"
    
    

    // ------ Select
    
    static let getAllMember : String = "SELECT * from member;"
    
    static let getAllUser : String = "SELECT * from user;"
    
    static let getAllFamily : String = "SELECT * from family;"
    
    static let getAuthenticate : String = "SELECT * from authenticate;"
    
    // ------- Count
    
    static let countMember : String = "SELECT COUNT (*) from member;"
    
    static let countUser : String = "SELECT COUNT (*) from user;"
    
    static let countAuthenticate : String = "SELECT COUNT (*) from authenticate;"
    
    // ----- Delete
    
    static let deleteFamily : String = "DELETE FROM family;"
    
    static let deleteUser : String = "DELETE FROM user;"
    
    static let deleteMember : String = "DELETE FROM member;"
    
    static let deleteAuthenticate: String = "DELETE FROM authenticate;"
    
    static let removeMember :String = "DELETE FROM member where userID = (?);"
    
    // ----- Insert
    
    static let addAuthenticate = "INSERT INTO authenticate (user, password) VALUES (?,?);"
    
    static let addFamily :String = "INSERT INTO family (familyId, name) VALUES (?,?);"
    
    static let addUser :String = "INSERT INTO user (firstName, lasname, userId) VALUES (?,?,?);"
    
    static let addMember :String = "INSERT INTO member (firstName, lasname, userId, latitude, longitude) VALUES (?, ?, ?, ?, ?);"

}
