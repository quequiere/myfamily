//
//  NetworkController.swift
//  MyFamily
//
//  Created by Supinfo on 02/12/2018.
//  Copyright © 2018 Supinfo. All rights reserved.
//

import UIKit

class NetworkController
{
    
    public static var online = false
    
    //Try to check internet settings and server availability
    static func refreshOnlineStatut(callback:@escaping () -> Void)
    {

        let totalStringRoute = "http://supinfo.steve-colinet.fr/supfamily/"
        let url = URL(string: totalStringRoute)!
        
        let task = URLSession.shared.dataTask(with: url)
        { (data, response, error) in
            if data != nil
            {
                NetworkController.online = true
                callback()
                return
            }
            else
            {
                NetworkController.online = false
                callback()
                return
            }
        }
        
        
        
        task.resume()
    }
}
