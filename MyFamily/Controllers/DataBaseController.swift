import SQLite3
import UIKit

class DataBaseController
{

    //Point to database
    static var dataBase: OpaquePointer?
    
    //magic thing to insert data propely in db
    static internal let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    
    static func initializeDataBase()
    {
        let dbFile = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("MyFamily.sqlite")
        
         if sqlite3_open(dbFile.path, &dataBase) != SQLITE_OK {
            print("Can't access to databse file")
            return
        }
        
        createTablesFromString(query: DataBaseQuery.familyTableCreate)
        createTablesFromString(query: DataBaseQuery.userTableCreate)
        createTablesFromString(query: DataBaseQuery.memberTableCreate)
        createTablesFromString(query: DataBaseQuery.authenticateTableCreate)
        
    }
    
    //check if database has data
    static func databaseHasData() -> Bool
    {
        if getAuthenticateCount()>0
        {
            return true
        }

        return false
    }
    
    
    
    //create all tables if necessary
    static func createTablesFromString(query : String)
    {
        if sqlite3_exec(dataBase, query, nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(dataBase)!)
            print("Error occured when create table: \(errmsg)")
        }
    }

    
    //authenticate data
    static func getAuthenticateCount() -> Int
    {
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.countAuthenticate, -1, &stmt, nil)
        sqlite3_step(stmt)
        return Int(sqlite3_column_int(stmt, 0))
    }

    
    //count members in db
    static func getMembersCount() -> Int
    {
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.countMember, -1, &stmt, nil)
        sqlite3_step(stmt)
        return Int(sqlite3_column_int(stmt, 0))
    }
    
    //count user in db should return 1 or 0
    static func getUserCount() -> Int
    {
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.countUser, -1, &stmt, nil)
        sqlite3_step(stmt)
        return Int(sqlite3_column_int(stmt, 0))
    }
    
    static func getUser() -> User
    {
        let userCount = getUserCount()
        if userCount <= 0 {
            print("Can't find user in database, actual user: \(userCount)")
        }
        
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.getAllUser, -1, &stmt, nil)
        sqlite3_step(stmt)
        let fname = String(cString: sqlite3_column_text(stmt, 1))
        let lname = String(cString: sqlite3_column_text(stmt, 2))
        let idm = Int(sqlite3_column_int(stmt, 3))
        
            
        return User(userId : idm, lasName : lname, firstName : fname)
    }
    
    static func getFamily() -> Family
    {
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.getAllFamily, -1, &stmt, nil)
        sqlite3_step(stmt)
        
        let idm = Int(sqlite3_column_int(stmt, 1))
        let famName = String(cString: sqlite3_column_text(stmt, 2))
        
        return Family(id : idm, name : famName)
    }
    
    
    static func getAuthenticate() -> [String]
    {
        var values = [String]()
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.getAuthenticate, -1, &stmt, nil)
        sqlite3_step(stmt)
        
        values.append(String(cString: sqlite3_column_text(stmt, 1)))
        values.append(String(cString: sqlite3_column_text(stmt, 2)))
        
        return values
    }
    
    //get all members from db and return an array
    static func getMembers() -> [Member]
    {
        var members = [Member]()
        
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.getAllMember, -1, &stmt, nil)
        while(sqlite3_step(stmt) == SQLITE_ROW){
            let fname = String(cString: sqlite3_column_text(stmt, 1))
            let lname = String(cString: sqlite3_column_text(stmt, 2))
            let idm = Int(sqlite3_column_int(stmt, 3))
         
            var lat : Double? = nil
            if(sqlite3_column_type(stmt, 4) != SQLITE_NULL)
            {
                 lat = sqlite3_column_double(stmt, 4)
            }
            
            var longi : Double? = nil
            if(sqlite3_column_type(stmt, 5) != SQLITE_NULL)
            {
                longi = sqlite3_column_double(stmt, 5)
            }
            let m = Member(firstName: fname, lastName: lname, id: idm, latitude: lat, longitude: longi)
            
            members.append(m)
        }
        
        return members
    }
    
    // write all temp data to db
    static func writeAllDataToDataBase(clearAuth : Bool)
    {
        deleteAllRows(clearAuth: clearAuth)
        
        registerUser(user: FamilyController.getUser())
        
        registerFamily(family: FamilyController.getFamily())
        
        refreshMemberToDatabase()
  
    }
    
    //Write member cache data do db
    static func refreshMemberToDatabase()
    {
        deleteMembers()
        for m in FamilyController.getFamily().getMembers(){
            registerMemberToDB(member: m)
        }
    }
    
    
    //register credidentials
    static func registerCredidentials(user : String, password: String)
    {
        
        deleteAuthenticate()
        
        var stmt: OpaquePointer?
        
        sqlite3_prepare_v2(dataBase, DataBaseQuery.addAuthenticate, -1, &stmt, nil)
        
        sqlite3_bind_text(stmt, 1,user, -1, SQLITE_TRANSIENT)
        sqlite3_bind_text(stmt, 2, password, -1, SQLITE_TRANSIENT)

        sqlite3_step(stmt)
        
    }
    
    //register a member du DB
    static func registerMemberToDB(member : Member)
    {
       
        var stmt: OpaquePointer?
        
        sqlite3_prepare_v2(dataBase, DataBaseQuery.addMember, -1, &stmt, nil)

        sqlite3_bind_text(stmt, 1,member.firstName!, -1, SQLITE_TRANSIENT)
        sqlite3_bind_text(stmt, 2, member.lasName!, -1, SQLITE_TRANSIENT)
        sqlite3_bind_int(stmt, 3, Int32(member.userId!))

        if member.latitude != nil && member.longitude != nil
        {
            sqlite3_bind_double(stmt, 4, member.latitude!)
            sqlite3_bind_double(stmt, 5, member.longitude!)
        }
        
        sqlite3_step(stmt)
    }

    //Register a family to db, 1 row only
    static func registerFamily(family : Family)
    {
        deleteFamily()
        
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.addFamily, -1, &stmt, nil)
        sqlite3_bind_int(stmt, 1, Int32(family.id!))
        sqlite3_bind_text(stmt, 2, family.name, -1, SQLITE_TRANSIENT)
        sqlite3_step(stmt)
    }
    
    //register user to db, 1 row only
    static func registerUser(user : User)
    {
        deleteUser()
        
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.addUser, -1, &stmt, nil)
        sqlite3_bind_text(stmt, 1, user.firstName, -1, SQLITE_TRANSIENT)
        sqlite3_bind_text(stmt, 2, user.lasName, -1, SQLITE_TRANSIENT)
        sqlite3_bind_int(stmt, 3, Int32(user.userId!))
        sqlite3_step(stmt)
    }
    
    //delete all user from db
    private static func deleteUser()
    {
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.deleteUser, -1, &stmt, nil)
        sqlite3_step(stmt)
    }
    
    //delete family from db
    private static func deleteFamily()
    {
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.deleteFamily, -1, &stmt, nil)
        sqlite3_step(stmt)
    }
    
    //delete family from db
    private static func deleteMembers()
    {
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.deleteMember, -1, &stmt, nil)
        sqlite3_step(stmt)
    }
    
    private static func deleteAuthenticate()
    {
        var stmt: OpaquePointer?
        sqlite3_prepare(dataBase, DataBaseQuery.deleteAuthenticate, -1, &stmt, nil)
        sqlite3_step(stmt)
    }
    
    //delete all row from all table from db
    public static func deleteAllRows(clearAuth : Bool)
    {
        if clearAuth
        {
            deleteAuthenticate()
        }
        
        
        deleteFamily()
        deleteMembers()
        deleteUser()
    }


}
