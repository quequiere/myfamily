//
//  CredidentialController.swift
//  MyFamily
//
//  Created by Supinfo on 01/12/2018.
//  Copyright © 2018 Supinfo. All rights reserved.
//
import UIKit

public class CredidentialController
{
    public static var user : String = ""
    public static var password : String = ""
    
    static func getIdentityString() -> String
    {
        return "&username=\(CredidentialController.user)&password=\(CredidentialController.password)"
    }
    

    //Logout the user and kill the app
    public static func logOut()
    {
        
        let url = URL(string: "http://supinfo.steve-colinet.fr/supfamily/?action=logout\(CredidentialController.getIdentityString())")!
        let jsonDecoder = JSONDecoder()
        let task = URLSession.shared.dataTask(with: url)
        { (data, response, error) in
            if let data = data
            {
                if let result = try? jsonDecoder.decode(ResultLogin.self, from: data)
                {
                    if(result.success != nil && result.success!)
                   {
                        print("Sucessfuly logout")
                    }
                    else
                   {
                        print("Something go wrong during logout, but data local data will removed")
                    }
                    
                    killAndClear()
                }
                else
                {
                    print ("Fail to parse object")
                    killAndClear()
                }
            }
            else
            {
                print ("Fail to get data")
                killAndClear()
            }
        }

        task.resume()
     
    }
    
    static func killAndClear()
    {
        DataBaseController.deleteAllRows(clearAuth: true)
        exit(0)
    }
}

