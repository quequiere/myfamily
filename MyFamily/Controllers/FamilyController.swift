//
//  FamilyController.swift
//  MyFamily
//
//  Created by Supinfo on 28/11/2018.
//  Copyright © 2018 Supinfo. All rights reserved.
//

import UIKit


public class FamilyController
{
    
    //Singleton patern, always return a value, or instantiate it if nil
    private static var jsonFamilyDto : JsonFamilyObject? = nil
    
    
    
    //Singleton getter
    static func getUser() -> User
    {
        if(jsonFamilyDto?.user == nil)
        {
            print("User is not loaded")
        }
        
        return jsonFamilyDto?.user as User!
        
    }
    
    
    //Singleton getter
    static func getFamily() -> Family
    {
        if(jsonFamilyDto?.family == nil)
        {
            print("Family is not loaded")
        }

        return jsonFamilyDto?.family as Family!
 
    }
    
    //Try to log user with credidential, and return if success or not, asynchronicly
    static func tryToLog(user : String?, password : String? ,callback:@escaping (_ success : Bool) -> Void)
    {
        
        if user==nil || password==nil || (user?.isEmpty)! || (password?.isEmpty)!
        {
            callback(false)
            return
        }
        
        //Construct auth http route then send it to authentificate
        let tempAuthString = "&username=\(user!)&password=\(password!)"
        let totalStringRoute = "http://supinfo.steve-colinet.fr/supfamily/?action=login\(tempAuthString)"
        let url = URL(string: totalStringRoute)!
        
        
        let jsonDecoder = JSONDecoder()
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data
            {
                if let jsonBo = try? jsonDecoder.decode(JsonFamilyObject.self, from: data)
                {
                    if jsonBo.user != nil
                    {
                        print("Success login")
                        //Store the password and user name
                        jsonFamilyDto=jsonBo
                        CredidentialController.user=user!
                        CredidentialController.password=password!
                        DataBaseController.registerCredidentials(user: user!, password: password!)
                        callback(true)
                    }
                    else
                    {
                        print ("Fail to get user data")
                        callback(false)
                    }
                }
                else
                {
                    print ("Fail to parse object")
                    callback(false)
                }
            }
            else
            {
                print("data not load")
                callback(false)
            }
        }
        
        task.resume()
    }
    
    //Reload the family from local data
    static func reloadFamilyFromLocal(callback:@escaping () -> Void)
    {
        let familyObj : JsonFamilyObject = JsonFamilyObject()
        
        familyObj.family =  DataBaseController.getFamily()
        familyObj.family?.members = DataBaseController.getMembers()
        familyObj.user = DataBaseController.getUser()
        
        jsonFamilyDto = familyObj
        
        callback()
        
    }
    
    //Reload the family data, include location
    static func reloadFamilyMembers(callback:@escaping () -> Void)
    {
        let url = URL(string: "http://supinfo.steve-colinet.fr/supfamily/?action=getPosition\(CredidentialController.getIdentityString())")!
        let jsonDecoder = JSONDecoder()
        let task = URLSession.shared.dataTask(with: url)
        { (data, response, error) in
            if let data = data
            {
                if let familyBo = try? jsonDecoder.decode(JsonFamilyObject.self, from: data)
                {
                    jsonFamilyDto?.redefineFamily(updated: familyBo.family!)
                    DataBaseController.writeAllDataToDataBase(clearAuth: false)
                    callback()
                }
                else
                {
                    print ("Fail to parse object")
                }
            }
            else
            {
                print ("Fail to get data")
            }
        }
        task.resume()
    }

    
    static func getNewMemberId() -> Int
    {
        var id = 0
        for m in getFamily().getMembers()
        {
            if(m.userId!>=id)
            {
                id = m.userId!+1
            }
        }
        return id
    }

    

}

