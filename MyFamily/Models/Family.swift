import UIKit

class Family : Codable {
	let id : Int?
	var name : String?
    var members : [Member]?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case members = "members"
	}

	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		members = try values.decodeIfPresent([Member].self, forKey: .members)
	}
    
    required init (id : Int , name : String)
    {
        self.id = id
        self.name = name
    }
    
    func getMember(pos:Int) -> Member
    {
        return members![pos]
    }
    
    func removeMember(pos:Int)
    {
        members?.remove(at: pos)
        DataBaseController.refreshMemberToDatabase()
    }
    
    func addMember(member : Member)
    {
        members?.append(member)
        DataBaseController.refreshMemberToDatabase()
    }
    
    //Get all members
    func getMembers() -> [Member]
    {
     
        if let memb = self.members
        {
            return memb
        }
        else
        {
            return Array<Member>()
        }
        
    }
    


}
