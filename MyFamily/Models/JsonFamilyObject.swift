class JsonFamilyObject : Codable {
    
	var family : Family?
    var user : User?

	enum CodingKeys: String, CodingKey {

		case family = "family"
        case user = "user"
	}

    required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		family = try values.decodeIfPresent(Family.self, forKey: .family)
        user = try values.decodeIfPresent(User.self, forKey: .user)
	}
    
    required init() {
        family = nil
        user = nil
    }
    
    func redefineFamily(updated : Family)
    {
        print("Family object was updated")
        self.family=updated
    }


}
