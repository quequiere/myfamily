
struct ResultLogin : Codable {
	let success : Bool?

	enum CodingKeys: String, CodingKey {

		case success = "success"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
	}

}
