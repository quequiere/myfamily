

struct User : Codable {
	let userId : Int?
	let lasName : String?
	let firstName : String?

	enum CodingKeys: String, CodingKey {

		case userId = "userId"
		case lasName = "lasName"
		case firstName = "firstName"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		userId = try values.decodeIfPresent(Int.self, forKey: .userId)
		lasName = try values.decodeIfPresent(String.self, forKey: .lasName)
		firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
	}
    
    init(userId : Int, lasName: String, firstName: String)
    {
        self.userId=userId
        self.lasName = lasName
        self.firstName = firstName
    }

}
