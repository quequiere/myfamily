class Member : Codable {
	let userId : Int?
	let lasName : String?
	let firstName : String?
    let latitude : Double?
    let longitude : Double?

	enum CodingKeys: String, CodingKey {

		case userId = "userId"
		case lasName = "lasName"
		case firstName = "firstName"
        case latitude = "latitude"
        case longitude = "longitude"
	}

	 required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		userId = try values.decodeIfPresent(Int.self, forKey: .userId)
		lasName = try values.decodeIfPresent(String.self, forKey: .lasName)
		firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
	}
    
    required init (firstName : String ,lastName : String)
    {
        self.firstName = firstName
        self.lasName = lastName
        self.userId = FamilyController.getNewMemberId()
        self.latitude = nil
        self.longitude = nil
    }
    
    required init (firstName : String ,lastName : String, id : Int, latitude : Double?, longitude : Double?)
    {
        self.firstName = firstName
        self.lasName = lastName
        self.userId = id
        self.latitude = latitude
        self.longitude = longitude
    }
    
    
    func getName() -> String
    {
        return "#\(self.userId!) \(self.firstName!) \(self.lasName!)"
    }
    
    

}
